﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_26._2___TV
{
    internal class Warmwaterkoker : ElektrischToestel
    {
        private decimal _inhoud;

        public Warmwaterkoker() { }
        public Warmwaterkoker(string merk, string type, string afbeelding, decimal inhoud) : base(merk, type, afbeelding)
        {
            this.Inhoud = inhoud;
        }

        public decimal Inhoud
        {
            get { return _inhoud; }
            set { _inhoud = value; }
        }

        public override string ToString()
        {
            return base.ToString() + $"Inhoud: {Inhoud}";
        }
    }
}
