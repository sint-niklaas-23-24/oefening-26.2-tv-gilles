﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_26._2___TV
{
    internal class ElektrischToestel
    {
        private string _afbeelding;
        private string _merk;
        private bool _power;
        private string _type;

        public ElektrischToestel() { }
        public ElektrischToestel(string merk, string type, string afbeelding)
        {
            this.Merk = merk;
            this.Type = type;
            this.Afbeelding = afbeelding;
        }

        public string Afbeelding
        {
            get { return _afbeelding; }
            set { _afbeelding = value; }
        }
        public string Merk
        {
            get { return _merk; }
            set { _merk = value; }
        }
        public string Type 
        {
            get { return _type; }
            set { _type = value; }
        }
        public bool Power
        {
            get { return _power; }
            set { _power = value; }
        }

        public override string ToString()
        {
            return $"Merk: {Merk}{Environment.NewLine}Type: {Type}{Environment.NewLine}Power: {Power}{Environment.NewLine}";
        }
    }
}
