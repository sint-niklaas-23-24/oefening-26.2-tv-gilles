﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Oefening_26._2___TV
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        List<Warmwaterkoker> lijstKokers = new List<Warmwaterkoker>();
        TV Sony = new TV("Sony", "KD-77AG9", 100, 77, "/SonyTv.jpg");
        TV Samsung = new TV("Samsung", "QN90C", 144, 89, "/SamsungTv.jpg");
        Warmwaterkoker tefal = new Warmwaterkoker("Tefal", "Performa 2KI110D Express+ 1,7L RVS", "/Tefalkoker.jpg", Convert.ToDecimal(1.7));
        Warmwaterkoker philips = new Warmwaterkoker("Philips", "Philips Daily Collection HD9350/90", "/Philipskoker.jpg", Convert.ToDecimal(1.7));

        private void rdoSony_Checked(object sender, RoutedEventArgs e)
        {
            txtKanaal.Text = null;
            txtVolume.Text = null;
            lblDisplayTV.Content = Sony.ToString();
            imageBox.Source = new BitmapImage(new Uri(Sony.Afbeelding, UriKind.Relative));
        }

        private void rdoSamsung_Checked(object sender, RoutedEventArgs e)
        {
            txtKanaal.Text = string.Empty;
            txtVolume.Text = string.Empty;
            lblDisplayTV.Content = Samsung.ToString();
            imageBox.Source = new BitmapImage(new Uri(Samsung.Afbeelding, UriKind.Relative));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            rdoSony.IsChecked = true;
            cbxPowerTV.IsChecked = true;
            lijstKokers.Add(tefal);
            lijstKokers.Add(philips);
            cmbWaterk.Items.Add(tefal.Type);
            cmbWaterk.Items.Add(philips.Type);
            cmbWaterk.SelectedIndex = 0;
        }

        private void txtKanaal_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                if (cbxPowerTV.IsChecked == true)
                {
                    if (String.IsNullOrEmpty(txtKanaal.Text) == false && int.TryParse(txtKanaal.Text, out int resultaat) == true)
                    {
                        if (rdoSamsung.IsChecked == true)
                        {
                            Samsung.Kanaal = resultaat;
                            lblDisplayTV.Content = Samsung.ToString();
                        }
                        else if (rdoSony.IsChecked == true)
                        {
                            Sony.Kanaal = resultaat;
                            lblDisplayTV.Content = Sony.ToString();
                        }
                    }
                    else if (!String.IsNullOrEmpty(txtKanaal.Text) && int.TryParse(txtKanaal.Text, out int value) == false)
                    {
                        throw new FormatException("U heeft geen integrale numerieke waarde voor het kanaal ingegeven.");
                    }
                }
            }
            catch (FormatException fe)
            {
                MessageBox.Show(fe.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtVolume_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                if (cbxPowerTV.IsChecked == true)
                {
                    if (String.IsNullOrEmpty(txtVolume.Text) == false && int.TryParse(txtVolume.Text, out int resultaat) == true)
                    {
                        if (rdoSamsung.IsChecked == true)
                        {
                            Samsung.Volume = resultaat;
                            lblDisplayTV.Content = Samsung.ToString();
                        }
                        else if (rdoSony.IsChecked == true)
                        {
                            Sony.Volume = resultaat;
                            lblDisplayTV.Content = Sony.ToString();
                        }
                    }
                    else if (!String.IsNullOrEmpty(txtVolume.Text) && int.TryParse(txtVolume.Text, out int value) == false)
                    {
                        throw new FormatException("U heeft geen integrale numerieke waarde voor het volume ingegeven.");
                    }
                }
            }
            catch (FormatException fe)
            {
                MessageBox.Show(fe.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cbxPowerTV_Checked(object sender, RoutedEventArgs e)
        {
            if (rdoSamsung.IsChecked == true)
            {
                Samsung.Power = true;
                lblDisplayTV.Content = Samsung.ToString();
            }
            else if (rdoSony.IsChecked == true)
            {
                Sony.Power = true;
                lblDisplayTV.Content = Sony.ToString();
            }
            lblVolume.IsEnabled = true;
            lblKanaal.IsEnabled = true;
            txtKanaal.IsEnabled = true;
            txtVolume.IsEnabled = true;
        }

        private void cbxPowerTV_Unchecked(object sender, RoutedEventArgs e)
        {
            if (rdoSamsung.IsChecked == true)
            {
                Samsung.Volume = 0;
                Samsung.Kanaal = 0;
                Samsung.Power = false;
                lblDisplayTV.Content = Samsung.ToString();
            }

            else if (rdoSony.IsChecked == true)
            {
                Sony.Kanaal = 0;
                Sony.Volume = 0;
                Sony.Power = false;
                lblDisplayTV.Content = Sony.ToString();
            }
            lblVolume.IsEnabled = false;
            lblKanaal.IsEnabled = false;
            txtKanaal.IsEnabled = false;
            txtVolume.IsEnabled = false;
            txtKanaal.Clear();
            txtVolume.Clear();
        }

        private void cbxPowerWaterkoker_Checked(object sender, RoutedEventArgs e)
        {
            if (cmbWaterk.SelectedIndex == 0)
            {
                lijstKokers[0].Power = true;
                lblDisplayWaterkoker.Content = lijstKokers[0].ToString();
            }
            else if (cmbWaterk.SelectedIndex == 1)
            {
                lijstKokers[1].Power = true;
                lblDisplayWaterkoker.Content = lijstKokers[1].ToString();
            }
        }

        private void cbxPowerWaterkoker_Unchecked(object sender, RoutedEventArgs e)
        {
            if (cmbWaterk.SelectedIndex == 0)
            {
                lijstKokers[0].Power = false;
                lblDisplayWaterkoker.Content = lijstKokers[0].ToString();
            }
            else if (cmbWaterk.SelectedIndex == 1)
            {
                lijstKokers[1].Power = false;
                lblDisplayWaterkoker.Content = lijstKokers[1].ToString();
            }
        }

        private void cmbWaterk_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (cmbWaterk.SelectedIndex == 0)
            {
                imageBoxWaterKoker.Source = new BitmapImage(new Uri(lijstKokers[0].Afbeelding, UriKind.Relative));

                lblDisplayWaterkoker.Content = lijstKokers[0].ToString();
            }
            else if (cmbWaterk.SelectedIndex == 1)
            {
                lblDisplayWaterkoker.Content = lijstKokers[1].ToString();
                imageBoxWaterKoker.Source = new BitmapImage(new Uri(lijstKokers[1].Afbeelding, UriKind.Relative));
            }
        }
    }
}
