﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_26._2___TV
{
    internal class TV : ElektrischToestel
    {
        private int _beeldgrootte;
        private int _herz;
        private int _kanaal;
        private bool _teletekst;
        private int _volume;

        public TV() { }
        public TV(string merk, string type, int herz, int beeldgrootte, string afbeelding) : base(merk, type, afbeelding) 
        {
            this.Herz = herz;
            this.Beeldgrootte = beeldgrootte;
        }

        public int Herz
        {
            get { return _herz; }
            set { _herz = value; }
        }
        public int Beeldgrootte
        {
            get { return _beeldgrootte; }
            set { _beeldgrootte = value; }
        }
        public int Kanaal 
        { 
            get { return _kanaal; }
            set {  _kanaal = value; }
        }
        public int Volume
        {
            get { return _volume; }
            set { _volume = value; }
        }
        public bool Teletekst
        {
            get { return _teletekst; }
            set { _teletekst = value; }
        }

        public override string ToString()
        {
            return base.ToString() + $"Herz: {Herz}{Environment.NewLine}Beeldgrootte: {Beeldgrootte}{Environment.NewLine}Kanaal: {Kanaal}{Environment.NewLine}Volume: {Volume}";
        }
    }
}
